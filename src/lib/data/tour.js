import Shepherd from 'shepherd.js';

export const createTour = () => {
    localStorage.getItem('mainPageTourComplete');
    if(localStorage.getItem('mainPageTourComplete')) return
	const tour = new Shepherd.Tour({
		useModalOverlay: true,
        
		cancelIcon: {
			enabled: true
		},
		scrollTo: { behavior: 'smooth', block: 'center' },
		defaultStepOptions: {
			classes: 'taskTalesTour',
			scrollTo: true
		}
	});
	tour.addStep({
		text: `Добро пожаловать в Task Tales! Ваше приключение начинается с вашего профиля. Здесь вы видите свою аватарку, текущий уровень и опыт. С каждой выполненной задачей ваш герой становится сильнее!`,
		attachTo: {
			element: '#tour-step-1',
			on: 'bottom'
		},
		buttons: [
			{
				text: 'Дальше',
				action: tour.next
			}
		],
		scrollTo: false
	});
	tour.addStep({
		text: 'Впереди вас ждут многочисленные квесты и задания. В этом блоке вы найдете свой текущий список дел. Готовы начать?',
		attachTo: {
			element: '#tour-step-2',
			on: 'top'
		},
		buttons: [
			{
				text: 'Дальше',
				action: tour.next
			}
		],
		scrollTo: true
	});
	tour.addStep({
		text: 'Чтобы добавить задачу, просто введите её в это поле. Каждая задача - это новый квест для вашего героя.',
		attachTo: {
			element: '#tour-step-3',
			on: 'top'
		},
		buttons: [
			{
				text: 'Дальше',
				action: tour.next
			}
		],
		scrollTo: true
	});
	tour.addStep({
		text: 'Здесь вы найдете все свои текущие задачи. Не забывайте отмечать их выполнение, чтобы ваш герой продвигался вперед.',
		attachTo: {
			element: '#tour-step-4',
			on: 'top'
		},
		buttons: [
			{
				text: 'Дальше',
				action: tour.next
			}
		],
		scrollTo: true
	});
	tour.addStep({
		id: 'example-step',
		text: 'Откройте карту приключений и посмотрите, какие квесты ждут вас в разных локациях. Каждая локация - это новый шанс набрать больше опыта.',
		attachTo: {
			element: '#tour-step-5',
			on: 'top'
		},
		buttons: [
			{
				text: 'Дальше',
				action: tour.next
			}
		],
		scrollTo: true
	});
	tour.addStep({
		text: 'Здесь вы можете узнать больше о навыках вашего героя и вашем текущем прогрессе. С каждым выполненным квестом вы будете улучшать свои навыки.',
		attachTo: {
			element: '#tour-step-6',
			on: 'top'
		},
		buttons: [
			{
				text: 'Дальше',
				action: tour.next
			}
		],
		scrollTo: true
	});
	tour.addStep({
		text: 'Хотите подробнее узнать о своем персонаже? Вам сюда.',
		attachTo: {
			element: '#tour-step-7',
			on: 'top'
		},
		buttons: [
			{
				text: 'Закончить',
				action: ()=>{
                    tour.complete();
                    localStorage.setItem('mainPageTourComplete', 'true');
                }
			}
		],
		scrollTo: true
	});
	tour.start();
};
