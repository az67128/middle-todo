export default {
	'a7b25d1d-3617-407f-921c-4c717663f92b': {
		name: 'hauntedmoors',
		image: 'hauntedmoors',
		places: [
			{ id: 1, x: 157, y: 246, name: 'Дом', pulse: true },
			{
				id: 2,
				x: 303,
				y: 365,
				name: 'Таверна «Золотой Рог»',
				quest_id: 'a85df173-9420-4378-b021-15937bf74594'
			},
			{
				id: 3,
				x: 216,
				y: 586,
				name: 'Коттедж "Зеленый Лист"',
				quest_id: '039d89b1-aede-4a03-9e8d-efd81527fed5'
			},
			{
				id: 4,
				x: 900,
				y: 805,
				name: 'Дом на болоте',
				nameX: 60,
				quest_id: '2885a17c-9963-48bc-b781-4b674603a175'
			},
			// {
			// 	id: 5,
			// 	x: 552,
			// 	y: 957,
			// 	name: 'К бескрайним степям',
			// 	nameX: 100,
			// 	action: {
			// 		type: 'move_to_map',
			// 		targetMapId: 'c1552f3a-9591-47aa-bb91-bc3f6fccf2ba',
			// 		targetPlaceId: 6
			// 	}
			// },
			{
				id: 6,
				x: 398,
				y: 144,
				name: 'Ферма Голубой Луны',
				quest_id: '9d033b62-79c0-46d9-93d8-0daa5e607488'
			},
			{
				id: 7,
				x: 713,
				y: 214,
				name: 'Гостиница "Серебряная звезда"',
				nameX: 170,
				quest_id: 'e7a487f8-47b3-49b6-b621-f35d39d9599b'
			},
			{
				id: 8,
				x: 895,
				y: 371,
				name: 'К Светлозорью',
				nameX: 80,
				action: {
					type: 'move_to_map',
					targetMapId: '9bd4b0e3-32d5-44bf-8bea-4ad637bcc008',
					targetPlaceId: 1
				}
			}
		],
		connections: [
			{ from: 1, to: 2 },
			{ from: 2, to: 3 },
			{ from: 2, to: 4 },
			// { from: 3, to: 5 },
			{ from: 2, to: 6 },
			{ from: 6, to: 7 },
			{ from: 7, to: 8 }
		]
	},
	'c1552f3a-9591-47aa-bb91-bc3f6fccf2ba': {
		name: 'endlesssteppes',
		image: 'endlesssteppes',
		places: [
			{
				id: 6,
				x: 552,
				y: 44,
				name: 'Дом',
				action: {
					type: 'move_to_map',
					targetMapId: 'a7b25d1d-3617-407f-921c-4c717663f92b',
					targetPlaceId: 5
				}
			},
			{
				id: 7,
				x: 421,
				y: 211,
				name: 'Таверна Золотой Рог'
			},
			{ id: 8, x: 380, y: 452, name: 'Дом Торина' },
			{ id: 9, x: 934, y: 471, name: 'К вересковым пустошам', nameX: 170 }
		],
		connections: [
			{ from: 6, to: 7 },
			{ from: 7, to: 8 },
			{ from: 8, to: 9 }
		]
	},
	'9bd4b0e3-32d5-44bf-8bea-4ad637bcc008': {
		name: 'hauntedmoorlands',
		image: 'hauntedmoorlands',
		places: [
			{
				id: 1,
				x: 68,
				y: 358,
				name: 'Дом',
				action: {
					type: 'move_to_map',
					targetMapId: 'a7b25d1d-3617-407f-921c-4c717663f92b',
					targetPlaceId: 8
				}
			},
			{
				id: 2,
				x: 329,
				y: 391,
				name: 'Церковь Отрады',
				quest_id: '4c15b379-4776-4ddb-be62-a251dd542a54',
				nameX: 80
			},
			{ id: 3, x: 522, y: 359, name: 'Уютное Гнездышко', nameX: 80, quest_id: '19d4a07d-7d1c-4b91-8da6-3c3d07c09cb5' },
			{ id: 4, x: 627, y: 246, name: 'Башня Горизонтов', nameX: 100, quest_id: 'e663ea49-37a6-4110-819f-3bd1c4a0751a' },
			{ id: 5, x: 685, y: 121, name: 'Хижина Тихого Уголка', nameX: 60, quest_id: '0cd53602-3053-41db-93b5-e1fbb53150b3' },
			{ id: 6, x: 881, y: 352, name: 'Усадьба Дубовых Врат', nameX: 130, quest_id: 'f79b522c-2cf1-4107-a7f2-eeab863bcff2' },
			{ id: 7, x: 747, y: 761, name: 'Седой особняк', nameX: 80, quest_id: '8145d6a2-1acd-4967-8704-060f433b4862' },
			{ id: 8, x: 886, y: 858, name: 'Дом Торина', nameX: 70, quest_id: '1de31223-0c38-456b-a6a7-16add6226de7' },
			{ id: 9, x: 170, y: 583, name: 'Ферма Луговых Рощ', nameX: 100, quest_id: 'b4afd3d6-908d-4db5-a14b-dfad8c60e611' }
		],
		connections: [
			{ from: 1, to: 2 },
			{ from: 2, to: 3 },
			{ from: 3, to: 4 },
			{ from: 4, to: 5 },
			{ from: 5, to: 6 },
			{ from: 6, to: 8 },
			{ from: 8, to: 7 },
			{ from: 7, to: 3 },
			{ from: 7, to: 9 },
			{ from: 9, to: 1 }
		]
	}
};
