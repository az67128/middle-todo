// src/stores/user.js

import { writable, get } from 'svelte/store';
import { getInitialValue } from '$lib/utils/ls.js';
import { browser } from '$app/environment';
import { auth } from './auth.js';
import { supabase } from './supabase.js';
import { popupStore } from './popup.js';
import * as Sentry from '@sentry/browser';

let isUserSet = false;
const defaultValue = {
	name: '',
	xp: 0,
	level: 1,
	next_level_xp: 100,
	avatar: 0,
	map: 'a7b25d1d-3617-407f-921c-4c717663f92b',
	place: 1
};
const initialValue = getInitialValue('player', defaultValue);
export const player = writable(initialValue || {});

export const fetchData = async () => {
	let { data, error } = await supabase
		.from('user')
		.select(`id, name, avatar, xp, level, next_level_xp, map, place`);
	if (error) {
		console.log('error', error);
	} else {
		player.set(data[0]);
		if (isUserSet) return;
		Sentry.setUser({ id: data[0]?.id, username: data[0]?.name });
		isUserSet = true;
	}
};

export const listenChange = () => {
	return supabase
		.channel('schema-db-changes')
		.on(
			'postgres_changes',
			{
				event: '*',
				schema: 'public',
				table: 'user'
			},
			() => {
				fetchData();
			}
		)
		.subscribe();
};

export const setupPlayer = async ({ name, avatar }) => {
	const { data } = await supabase
		.from('user')
		.upsert({
			id: get(auth)?.id,
			name,
			avatar,
			xp: 0,
			level: 1,
			next_level_xp: 100,
			map: 'a7b25d1d-3617-407f-921c-4c717663f92b',
			place: 1
		})
		.select();
	player.set(data[0]);
};

// Функция для добавления опыта
export async function addXP(amount) {
	player.update((state) => {
		state.xp += amount;

		// Проверка, хватает ли опыта для следующего уровня
		while (state.xp >= state.next_level_xp) {
			// Увеличиваем уровень
			state.level++;
			popupStore.set({
				image: 'newlevel',
				isOpen: true,
				header: 'ДОСТИГНУТ НОВЫЙ УРОВЕНЬ!',
				text: `Пусть этот новый уровень станет маяком в самые темные ночи, свидетельством вашей смелости и настойчивости. Но помните: предстоящее путешествие наполнено тайнами, которые еще предстоит разгадать, и врагами, которых еще предстоит победить. Идите вперед, ведь судьба Средиземья лежит на ваших плечах!`
			});

			// Уменьшаем текущий опыт на значение для достижения уровня
			state.xp -= state.next_level_xp;

			// Увеличиваем значение опыта для следующего уровня, например, на 50 очков больше
			state.next_level_xp += 50;
		}
		return state;
	});
	const state = get(player);
	try {
		await supabase
			.from('user')
			.update({ xp: state.xp, level: state.level, next_level_xp: state.next_level_xp })
			.eq('id', state.id);
	} catch (err) {
		console.log(err);
	}
}

if (browser) {
	player.subscribe(($player) => {
		window.localStorage.setItem('player', JSON.stringify($player));
	});
}
