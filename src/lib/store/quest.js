// src/stores/user.js

import { writable, get } from 'svelte/store';
import { browser } from '$app/environment';
import { supabase } from './supabase.js';
import { getInitialValue } from '$lib/utils/ls.js';
import { auth } from './auth.js';

const defaultValue = [];
const initialValue = getInitialValue('questList', defaultValue);

export const questList = writable(initialValue);

export const fetchQuestListData = async () => {
	let { data, error } = await supabase.from('quest').select(
		`quest_id, image, text, condition, complete, name, 
			quest_user (complete),
			quest_task (
				quest_task_id, name, description, skill_id, 
				skill ( id, name, icon ),
				userCompleted:quest_task_user_todo (created_at)
				)`
	);
	if (error) {
		console.log('error', error);
	} else {
		questList.set(data);
	}
};

if (browser) {
	questList.subscribe(($questList) => {
		window.localStorage.setItem('questList', JSON.stringify($questList));
	});
}

export const addUserQuest = async (quest_id) => {
	await supabase
		.from('quest_user')
		.insert({
			quest_id: quest_id,
			user_id: get(auth)?.id
		})
		.select()
		.single();
};
export const completeQuest = async (quest_id) => {
	await supabase.from('quest_user').update({ complete: true }).match({ quest_id: quest_id });
	await fetchQuestListData();
}

