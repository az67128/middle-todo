import { writable } from 'svelte/store';

const createNotificationStore = () => {
	const { subscribe, update } = writable([]);

	return {
		subscribe,
		add: (message, duration = 3000) => {
			const id = new Date().getTime();
			update((notifications) => [...notifications, { id, message, duration }]);
			setTimeout(() => {
				update((notifications) => notifications.filter((notification) => notification.id !== id));
			}, duration);
		}
	};
};

export const notifications = createNotificationStore();
