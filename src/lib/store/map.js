import { player } from './player.js';
import { derived, get } from 'svelte/store';
import { tweened } from 'svelte/motion';
import { supabase } from './supabase.js';
import { questList } from './quest.js';
import mapData from '$lib/data/map.js';

export const playerPosition = tweened({ x: 0, y: 0 });

export const map = derived([player, questList], ([$player, $questList]) => {
	const currentMap = mapData[$player.map];

	currentMap.places = currentMap.places.map((place) => {
		const quest = $questList ? $questList.find((q) => q.quest_id === place.quest_id) : null;

		if (!quest) return place;

		const allTasksCompleted = quest.quest_task.every(
			(task) => task.userCompleted && task.userCompleted.length > 0
		);
		const questCompleted = quest.quest_user[0]?.complete;

		return {
			...place,
			quest,
			allTasksCompleted: allTasksCompleted,
			questCompleted: questCompleted
		};
	});

	const currentPlace = currentMap.places.find((item) => item.id === $player.place) || currentMap.places[0];
	if (currentPlace?.x) {
		playerPosition.set({
			x: currentPlace.x,
			y: currentPlace.y
		}, get(playerPosition)?.x===0 ? {duration: 1} : {});
	}
	return { currentMap: currentMap, currentPlace: currentPlace };
});

export const setPlace = async (place) => {
	player.update((state) => ({ ...state, place }));
	await supabase.from('user').update({ place }).eq('id', get(player).id);
};
export const setMap = async (map, place) => {
	player.update((state) => ({ ...state, place, map }));
	const target = mapData[map].places.find((item) => item.id === place);

	playerPosition.set(
		{
			x: target.x,
			y: target.y
		},
		{ duration: 1 }
	);
	await supabase.from('user').update({ place, map }).eq('id', get(player).id);
};

