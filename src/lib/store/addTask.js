import { writable } from "svelte/store";

export const addTaskStore = writable({
    isFocused: false,
    inputValue: '',
    selectedSkill: '',
    frequency: 'daily',
    recurring: false,
    textarea: null,
})