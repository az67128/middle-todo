// src/stores/user.js

import { writable, get } from 'svelte/store';
import { browser } from '$app/environment';
import { supabase } from '$lib/store/supabase.js';
import { auth } from './auth.js';
import { getInitialValue } from '$lib/utils/ls.js';

const fetchTSkills = async () => {
	let { data, error } = await supabase.from('skill').select(`
		id, 
		name,
		icon,
		color,
		user_skill (xp, next_level_xp, level)
		`);
	if (error) {
		console.log('error', error);
	} else {
		skill.set(data);
	}
};
fetchTSkills();

export const listenChange = () => {
	return supabase
		.channel('schema-db-changes')
		.on(
			'postgres_changes',
			{
				event: '*',
				schema: 'public',
				table: 'skill'
			},
			() => {
				fetchTSkills();
			}
		)
		.subscribe();
};

const defaultValue = [];
const initialValue = getInitialValue('skill', defaultValue);

export const skill = writable(initialValue);

export async function addSkillXP(skill_id, amount = 10) {
	const upsert = async (targetSkill) => {
		await supabase
			.from('user_skill')
			.upsert({
				user_id: get(auth)?.id,
				skill_id: skill_id,
				level: targetSkill.level,
				xp: targetSkill.xp,
				next_level_xp: targetSkill.next_level_xp
			})
			.select();
	};
	skill.update((state) => {
		const skillIndex = state.findIndex((item) => item.id === skill_id);
		if (!state[skillIndex].user_skill.length) {
			state[skillIndex].user_skill = [{ xp: 0, next_level_xp: 100, level: 1 }];
		}
		const targetSkill = state[skillIndex].user_skill[0];
		targetSkill.xp += amount;
		while (targetSkill.xp >= targetSkill.next_level_xp) {
			targetSkill.level++;
			targetSkill.xp -= targetSkill.next_level_xp;
			targetSkill.next_level_xp += 50;
		}
		upsert(targetSkill, skill_id);
		// TODO async Function!

		return [...state];
	});
}

if (browser) {
	skill.subscribe(($skill) => {
		window.localStorage.setItem('skill', JSON.stringify($skill));
	});
}
