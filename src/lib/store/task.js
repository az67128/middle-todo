import { writable, get } from 'svelte/store';
import { browser } from '$app/environment';
import { supabase } from '$lib/store/supabase.js';
import { auth } from './auth.js';
import { v4 as uuid } from 'uuid';
import { addXP } from './player.js';
import { addSkillXP, skill } from './skill.js';
import { notifications } from './notification.js';
import { getInitialValue } from '$lib/utils/ls.js';

export const listenToTodoChange = () => {
	return supabase
		.channel('schema-db-changes')
		.on(
			'postgres_changes',
			{
				event: '*',
				schema: 'public',
				table: 'todo'
			},
			() => {
				fetchTodos();
			}
		)
		.subscribe();
};

const fetchTodos = async () => {
	let { data, error } = await supabase
		.from('todo')
		.select(
			`
		uuid,
		completed, 
		created_at,
		name,
		recurring,
		frequency,
		next_date,
		quest_task_id,
		skill ( id, name, icon )
	  `
		)
		.order('created_at', { ascending: false });
	if (error) {
		console.log('error', error);
	} else {
		tasks.set(data);
	}
};

export const deleteTodo = async (item) => {
	try {
		await supabase.from('todo').delete().eq('uuid', item.uuid);
		if (item.quest_task_id) {
			await supabase
				.from('quest_task_user_todo')
				.upsert({
					quest_task_id: item.quest_task_id,
					user_id: get(auth)?.id
				})
				.select()
				.single();
		}
	} catch (error) {
		console.log('error', error);
	}
};

const updateDate = async (id, date) => {
	try {
		await supabase.from('todo').update({ next_date: date }).eq('uuid', id);
	} catch (error) {
		console.log('error', error);
	}
};

let cleanupTimeout;
const defaultValue = [];
const initialTasks = getInitialValue('tasks', defaultValue);

export const tasks = writable(initialTasks);
fetchTodos();

export async function addTask({
	text,
	skillId = null,
	recurring = false,
	frequency = null,
	quest_task_id = null
}) {
	let task = text.trim();

	if (!task.length) return;
	const newId = uuid();

	let targetSkill = null;
	if (skillId) {
		targetSkill = get(skill).find((item) => item.id === skillId);
	}

	tasks.update((allTasks) => {
		return [
			{
				name: task,
				completed: false,
				uuid: newId,
				skill: targetSkill,
				recurring,
				frequency,
				next_date: recurring ? new Date() : null,
				quest_task_id
			},
			...allTasks
		];
	});

	try {
		await supabase
			.from('todo')
			.insert({
				name: task,
				user_id: get(auth)?.id,
				uuid: newId,
				skill: skillId || null,
				recurring: recurring || false,
				frequency: frequency || null,
				next_date: recurring ? new Date() : null,
				quest_task_id: quest_task_id || null
			})
			.select()
			.single();
		fetchTodos();
	} catch (err) {
		console.log(err);
	}
}
export async function updateTask({
	text,
	skillId = null,
	recurring = false,
	frequency = null,
	uuid
}) {
	await supabase
		.from('todo')
		.update({
			name: text,
			skill: skillId || null,
			recurring: recurring || false,
			frequency: frequency || null
		})
		.eq('uuid', uuid);
}


export function toggleTask(id) {
	clearTimeout(cleanupTimeout);
	tasks.update((allTasks) => {
		return allTasks.map((task) => {
			let nextDate = new Date();
			if (task.recurring) {
				switch (task.frequency) {
					case 'daily':
						nextDate.setHours(nextDate.getHours() + 20);
						break;
					case 'weekly':
						nextDate.setDate(nextDate.getDate() + 7);
						break;
					case 'monthly':
						nextDate.setMonth(nextDate.getMonth() + 1);
						break;
				}
			}

			return task.uuid === id
				? { ...task, completed: !!task.completed, next_date: task.recurring ? nextDate : null }
				: task;
		});
	});
	cleanupTimeout = setTimeout(() => {
		tasks.update((allTasks) => {
			return allTasks.filter((task) => {
				if (task.completed) {
					let notificationText = 'XP +10';
					addXP(10);
					if (task.skill?.id) {
						addSkillXP(task.skill.id, 10);
						notificationText += `; ${task.skill.name} +10`;
					}
					notifications.add(notificationText);
					if (task.recurring) {
						updateDate(task.uuid, task.next_date);
					} else {
						deleteTodo(task);
					}
				}
				return !task.completed || (task.recurring && new Date() > task.next_date);
			});
		});
	}, 2000);
}

if (browser) {
	tasks.subscribe(($tasks) => {
		window.localStorage.setItem('tasks', JSON.stringify($tasks));
	});
}
