import { browser } from '$app/environment';

export const getInitialValue = (key, defaultValue) => {
	let value = defaultValue;
	if (browser) {
		try {
			value = JSON.parse(window.localStorage.getItem(key));
		} catch (err) {}
	}
	return value;
};
